DEV_IMAGE=cmiller/py3-dev-env:$(shell head -1 py3-dev-env/VERSION | tr -d '\r\n')
OUTPUT_IMAGE=cmiller/py3-music-bot

.PHONY: all build image 
IMAGE_TAG=$(shell head -1 VERSION | tr -d '\r\n')

build:
	@docker run --rm -it -v $(PWD):/workspace $(DEV_IMAGE) /bin/bash -c "./build.sh"

image:
	@docker build -t $(OUTPUT_IMAGE):$(IMAGE_TAG) .

clean:
	@rm -rf *.spec build/ dist/
