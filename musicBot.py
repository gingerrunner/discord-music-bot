import asyncio
import os
import discord
from discord.ext import commands
import yt_dlp


class MusicBot(commands.Cog):
    # client and ffmpeg binary location are required, all else optional
    def __init__(self,
                 client,
                 ffmpeg_exe_location,
                 insult_gif=None,
                 join_gif=None,
                 leave_gif=None):
        self.client = client
        self.playlistQueue = []
        self.pauseBool = False
        self.joinBool = False
        self.ffmpegLocation = ffmpeg_exe_location
        self.insultGif = insult_gif
        self.joinGif = join_gif
        self.leaveGif = leave_gif
        self.YDL_OPTIONS = {
            'format': 'bestaudio/best',
            'extractaudio': True,
            'noplaylist': True,
            'keepvideo': False,
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '320'
            }]
        }
        self.youTube = yt_dlp.YoutubeDL(self.YDL_OPTIONS)
        self.helpMessage = "Commands:\n" + \
                           "$join                   -- Call the bot into the voice channel.\n" + \
                           "$disconnect     -- Kick the bot out of the voice channel.  You crazy, deranged, lunatic.\n" + \
                           "$play {YT url}  -- Plays audio from provided YouTube URL, if a song is already playing, it becomes the next one to play\n" + \
                           "$pause              -- Pause the music.\n" + \
                           "$resume           -- Resume the music.\n" + \
                           "$playlist          -- Displays the current queue of songs.\n" + \
                           "$skip                 -- Skip the current song playing, goes to the next one in the playlist if there is one.\n" + \
                           "$clear                -- Kill the current playlist of songs with fire.\n" + \
                           "$menu                -- Display this message." 

    # Have the bot join the voice channel you are in
    @commands.command()
    async def join(self, ctx):
        self.joinBool = True
        if ctx.author.voice is None:
            await ctx.send("You're not in a voice channel bro.", delete_after=10)
            if self.insultGif:
                await ctx.send(self.insultGif, delete_after=10)
            return
        voice_channel = ctx.author.voice.channel
        if ctx.voice_client is None:
            await voice_channel.connect()
        else:
            await ctx.voice_client.move_to(voice_channel)
        await ctx.send("Let's get this party started!", delete_after=10)
        if self.joinGif:
            await ctx.send(self.joinGif, delete_after=10)
        await self.main_loop(ctx)
    
    # Where the bot spends most of its time
    async def main_loop(self, ctx):
        vc = ctx.voice_client
        while self.joinBool:
            # Ensure the playlist has content, audio is not already playing, and we aren't paused
            while not ctx.voice_client.is_playing() and self.playlistQueue and not self.pauseBool:
                source = await discord.FFmpegOpusAudio.from_probe(self.playlistQueue[0]["file"], method='fallback')
                vc.play(source, after=self.sync_delete)
            # TODO: Could probably refactor to avoid this, just make everything happen on event via 
            # "after" callback in voiceClient play method, just not sure how to deal with ctx parameter 
            # since "after" callback requires only 1 parameter which is for errors
            # Rate we check if there is a song to play, seconds
            await asyncio.sleep(1)

    def sync_delete(self, error):
        os.remove(self.playlistQueue[0]["file"])
        self.playlistQueue.pop(0)

    # Disconnect the bot from the voice channel it has joined
    @commands.command()
    async def disconnect(self, ctx):
        self.joinBool = False
        await ctx.send("Imma hang glide out of this loser emporium!", delete_after=10)
        if self.leaveGif:
            await ctx.send(self.leaveGif, delete_after=10)
        await ctx.voice_client.disconnect()

    # Add song at a given YT URL to queue
    @commands.command()
    async def play(self, ctx, url):
        if not self.joinBool:
            await ctx.send("Please place the bot into a voice channel before queueing up songs.", delete_after=10)
            return
        info = self.youTube.extract_info(url, download=True)
        pieces = {"url": info["url"], "title": info["title"], 
            "file": info["requested_downloads"][0]["filepath"]}
        self.playlistQueue.append(pieces.copy())
    
    # Pause the bot's jams...
    @commands.command()
    async def pause(self, ctx):
        ctx.voice_client.pause()
        self.pauseBool = True
        await ctx.send("Pausing the fun...\n" + "loser", delete_after=10)

    # Resume the bot's jams...
    @commands.command()
    async def resume(self, ctx):
        ctx.voice_client.resume()
        self.pauseBool = False
        await ctx.send("DON'T STOP THE PARTY!", delete_after=10)

    # Skip current song, which can be done just by stopping the voice client, main loop
    # will take care of advancing
    @commands.command()
    async def skip(self, ctx):
        await ctx.send("*record scratch* next!", delete_after=10)
        ctx.voice_client.stop()

    # Clear the song queue
    @commands.command()
    async def clear(self, ctx):
        self.playlistQueue.clear()

    # Show the current playlist queue
    @commands.command()
    async def playlist(self, ctx):
        titleList = []
        for count, entry in enumerate(self.playlistQueue, start=1):
            titleList.append(str(count) + ". " + entry["title"])
        await ctx.send("Current playlist:\n" +
                       "---------------------------\n" +
                       '\n'.join(titleList), delete_after=15)

    # Display the "help" menu, help can't be used an alias though as it's already for something else
    @commands.command()
    async def menu(self, ctx):
        await ctx.send(self.helpMessage, delete_after=15)
