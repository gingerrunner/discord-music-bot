FROM debian:bookworm-slim

ENV DEBIAN_FRONTEND=noninteractive

USER root

# FFMPEG is needed by the bot
RUN apt-get update && apt-get install -y \
    ffmpeg && \
    rm -rf /var/lib/apt/lists/*

RUN addgroup musicbot && \
    useradd -ms /bin/bash user && \
    usermod -a -G musicbot user && \
    mkdir -p /app/bin/ && \
    chgrp -R musicbot /app/bin/ && \
    chmod -R g+w /app/bin/

COPY --chown=user:musicbot --chmod=0770 dist/ /app/bin/

USER user

# musicBot is name of folder inside dist that we need
WORKDIR /app/bin/musicBot
CMD ["./musicBot"]
