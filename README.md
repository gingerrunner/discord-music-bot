# discord-music-bot
Music bot based on an old YT tutorial I watched, added functionality over time and sought to make it 
easier to build and use without having to mess with a python environment much on Windows. You could of 
course run/set this up this locally if you wish and ignore the Docker stuff if you don't want to bother, 
but I intend this to be used with Docker for easier dependency management. 

[Demonstration](https://youtu.be/oshKn8aGkok)

## Setup
### Bot Creation
Make an application on the [discord developer portal](https://discord.com/developers/applications), 
fill out description and name as you desire.  
![newApp](docs/1newApp.png)  
Click the bot tab on the left side.  
![botTab](docs/2botTab.png)  
Uncheck "public bot", and check "Presence Intent", "Server Members Intent", and "Message Content Intent".  
![botIntents](docs/3botIntents.png)  
Copy bot's token and save it to a file of your chosing, do this via Reset/View Token (once token is reset you get to see it one time).  
NOTE: Save the file with a .token extension, as this repo has a .gitignore for it and if you choose to contribute you don't want to 
accidentally commit a token.  
Go to OAuth2 on left side.  
![oAuthTab](docs/4oAuthTab.png)
Check "bot" in the Scopes section.
![scopes](docs/5scopes.png)
Check "Administrator" in the General Permissions section.
![perms](docs/6perms.png)
Scroll down to Generated URL and go to it via browser, add it to your desired server.
![gen](docs/7generate.png)

### Building the Bot
This bot requires Docker, refer to [Docker's installation page](https://docs.docker.com/engine/install/) for your 
OS's installation guide. If you are on Windows, Docker for Desktop is a nice solution but you could also 
[enable WSL2](https://www.windowscentral.com/how-install-wsl2-windows-10) and install Docker there directly for your 
WSL2 Linux distro by referencing the Docker installation page above.  Then the following would just be done from within WSL2.
  
This uses a python developer image which can be built using the py3-dev-env submodule.
From the top level of repo:
```
git submodule update --init
cd py3-dev-env
make image
```
Once the dev image is built, from the top level of repo, call the following to build the discord music bot docker image:
```
make build image
```
### Running
Once the image is built, call the ./run.sh with the file with your bot token in it, the docker container 
when created will take the token in as an environment variable which will then be picked up by the bot. 
Once that's been done, run the bot as follows: 
From the top level of the repo:
```
./run.sh {path to your file with bot token in it}
```
Example:
```
./run.sh /path/to/tokenFile.token
```

## Using
The command prefix is $ as of time of writing to avoid collision with other bots. Before the bot can be used, 
the user calling it must be in the voice channel they want it in and use the $join command like so:  
```
$join
```
From there, $play commands can be used with the YouTube URL following it:  
```
$play https://www.youtube.com/watch?v=-d4XHnMAt6s
```
Currently the bot uses a queue system, if another song is queued while one is still playing, it will be next.

Use $menu to see other commands, don't want to overdocument here to require changing in multiple places when 
changes are made/features are added.  

## Code Flow
* Initiates bot
* Bot does nothing until it is joined with a user in voice chat
* Main loop runs in async event context checking every second for queued songs to play provided one isn't already played
* When play command is given a YouTube URL, the audio is taken from the video and saved to a file
* Song is played from file as opposed to live streaming
* When song is finished playing, file is deleted, queue is checked via main loop, repeat
    * While a song is playing, another one can be queued up, it will be downloaded while the current one is playing 
