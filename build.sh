#!/bin/bash

pip install -r requirements.txt --no-warn-script-location
pyinstaller --onedir --hidden-import=_cffi_backend -y --clean --name musicBot main.py musicBot.py
