import discord
from discord.ext import commands
from os import environ as env
import musicBot
import asyncio

ffmpegLocation = r'/usr/bin/ffmpeg' 
insultGif = r"![Moron](https://media.giphy.com/media/zKOqnQprdq2gU/giphy.gif)",
joinGif = r"![DaftPunk](https://media.giphy.com/media/sPtPHvqyANbuo/giphy.gif)",
leaveGif = r"![Terminator](https://media.giphy.com/media/LFEjnqmVmbKk8/giphy.gif)"
botToken = env.get("BOT_TOKEN", None)


async def setup(bot, cog):
    await bot.add_cog(cog)

if __name__ == "__main__":
    if not botToken:
        print("BOT_TOKEN environment variable not set, did you read the README?")
        sys.exit(1)
    client = commands.Bot(command_prefix='$', intents=discord.Intents.all())
    asyncio.run(setup(client, (musicBot.MusicBot(client, ffmpegLocation, insultGif, joinGif, leaveGif))))
    client.run(botToken)
