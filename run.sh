#!/bin/bash
VERSION=$(head -1 VERSION)
BOT_TOKEN=$(head -1 $1)
docker run --rm -it -e BOT_TOKEN=${BOT_TOKEN} cmiller/py3-music-bot:${VERSION}
